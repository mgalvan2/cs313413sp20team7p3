package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;
import java.util.List;
import java.util.ArrayList;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);  // No changes made
		return null;
	}

	@Override
	public Void onStrokeColor(final StrokeColor c) {
		final int temp = paint.getColor(); // Creates a copy of the current color
		paint.setColor(c.getColor());   // Updates the color to the argument
		c.getShape().accept(this);  // Takes a visitor and draws the shape with the new color
		paint.setColor(temp);  // Reverts the color to the original color
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
	    final Style temp = paint.getStyle();    // Original style value
		paint.setStyle(Style.FILL_AND_STROKE);  // Updates the style to both fill in and define a shape
		f.getShape().accept(this);
		paint.setStyle(temp);   // Reverts the style to the original one
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
	    for (Shape index: g.getShapes()) {  // Iterates over a List of shapes
	        index.accept(this);
	    }
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(), l.getY());   // Moves the location of where to draw the shape to a new coordinate
		l.getShape().accept(this);  // Draws a shape via a visitor
		canvas.translate(-l.getX(), -l.getY()); // Returns the location of where to draw shapes to original coordinates
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0,0,r.getWidth(), r.getHeight(), paint);    // Draws a rectangle
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		final Style temp = paint.getStyle();
		paint.setStyle(Style.STROKE);   // This seems like extraneous code considering that paint style is already set to Style.Stroke?
		o.getShape().accept(this);
		paint.setStyle(temp);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {
		final Point[] points = s.getPoints().toArray(new Point[s.getPoints().size()+1]);    // Create an array of points that are essentially the vertexes of the polygon
		assert(points.length >= 4); // Makes sure that the length of the array is not greater than four
		points[points.length-1] = points[0];    // Sets the last index to be equivalent to that of the first index

        final List<Float> float_list = new ArrayList<Float>();  // Creates a List of Floats
		for (int index = 0; index < points.length; index++) {   // For index in points.length, iterate through the list and
			float_list.add(new Float(points[index].getX()));    // Add the X coordinate to the list of floats
			float_list.add(new Float(points[index].getY()));    // Add the Y coordinate to the list of floats
		}

		final float[] floatArray = new float[float_list.size()];    // Create an array of floats of the same size as the List of floats
		int index = 0;
		for (Float f : float_list) {    // For each float in float_list add it to floatArray
		    if(f != null)   {
		        floatArray[index++] = f;
            }
		    else    {
		        floatArray[index++] = Float.NaN;
            }
		}
		canvas.drawLines(floatArray, paint);    // Draw the shape from the list of coordinates in floatArray
		return null;
	}
}
