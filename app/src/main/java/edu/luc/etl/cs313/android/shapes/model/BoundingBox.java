package edu.luc.etl.cs313.android.shapes.model;
import java.util.List;
/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);	// Returns a visitor to the Fill class
	}

	@Override
	public Location onGroup(final Group g) {
		int xCoord1 = Integer.MAX_VALUE;	// Used to run through group for finding max/min Bounding box of groups of objects
		int xCoord2 = Integer.MIN_VALUE;	// x1,y1
		int yCoord1 = Integer.MAX_VALUE;	// Then calculating width/height with x2,y2
		int yCoord2 = Integer.MIN_VALUE;
		final List <? extends Shape> shapeList = g.getShapes();

		for (Shape shape : shapeList) {             // Loop for running through shapes and determining shapes
			final Location boundingBox = shape.accept(this);
			final Rectangle rect = (Rectangle) boundingBox.getShape();

            xCoord1 = Math.min(xCoord1, boundingBox.getX());               //Looping to find max/min values of y/x and holding these values
            xCoord2 = Math.max(xCoord2, boundingBox.getX() + rect.getWidth());
            yCoord1 = Math.min(yCoord1, boundingBox.getY());
            yCoord2 = Math.max(yCoord2, boundingBox.getY() + rect.getHeight());
		}
		return new Location(xCoord1, yCoord1, new Rectangle(xCoord2 - xCoord1, yCoord2 - yCoord1));
	}

	@Override
	public Location onLocation(final Location l) {
		final int x = l.getX();
		final int y = l.getY();

		final Location location = l.getShape().accept(this);
		final int xCoord = location.getX();
		final int yCoord = location.getY();
		return new Location((x + xCoord), (y + yCoord), location.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		final int x = r.getWidth();
		final int y = r.getHeight();

		final int xCoord = (1/2) * x;       // Calculating start position based on width and height
		final int yCoord = (1/2) * y;
		return new Location(-xCoord, -yCoord, r);
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		int xCoord1 = Integer.MAX_VALUE;
		int xCoord2 = Integer.MIN_VALUE;
		int yCoord1 = Integer.MAX_VALUE;
		int yCoord2 = Integer.MIN_VALUE;
		final List<? extends Point> pointList = s.getPoints();
		for (Point point : pointList) {
            xCoord1 = Math.min(xCoord1, point.accept(this).getX());
            xCoord2 = Math.max(xCoord2, point.accept(this).getX());
            yCoord1 = Math.min(yCoord1, point.accept(this).getY());
            yCoord2 = Math.max(yCoord2, point.accept(this).getY());
		}

		for (Shape shape : pointList) {
			final Location boundingBox = shape.accept(this);
			final Rectangle rect = (Rectangle) boundingBox.getShape();

			xCoord1 = Math.min(xCoord1, boundingBox.getX());
			xCoord2 = Math.max(xCoord2, boundingBox.getX() + rect.getWidth());
			yCoord1 = Math.min(yCoord1, boundingBox.getY());
			yCoord2 = Math.max(yCoord2, boundingBox.getY() + rect.getHeight());
		}

		return new Location(xCoord1, yCoord1, new Rectangle((xCoord2 - xCoord1), (yCoord2 - yCoord1)));
	}
}
